/**
 * Table `user`
 *
 * @param {Integer}
 *            user_id
 * @param {String}
 *            username
 * @param {String}
 *            user_password
 * @param {String}
 *            user_email_confirmed
 * @param {String}
 *            user_email_confirmation_code
 * @param {String}
 *            user_first_name
 * @param {String}
 *            user_last_name
 * @param {String}
 *            user_create_dtime
 * @param {String}
 *            user_update_dtime
 *
 */

var crypto = require('crypto'), db = require('../lib/Database'), memcache = require('../lib/Memcache').mcClient, dateHelper = require('../helpers/DateHelper'), validateHelper = require('../helpers/ValidateHelper'), mainHelper = require('../helpers/MainHelper');

/**
 * Constructor
 *
 * @param {Object}
 *            params
 */
var User = function (params) {
    if (typeof params === 'undefined') {
        params = {};
    }

    this.loginUnf = false;
    this.dontCheck = {};
    this.user_id = null;

    if (typeof params.user_id !== 'undefined') {
        this.user_id = params.user_id;
    }

    if (typeof params.username !== 'undefined') {
        this.username = params.username;
    }

    if (typeof params.confirm_password !== 'undefined') {
        this.confirm_password = params.confirm_password;
    }

    if (typeof params.user_password !== 'undefined') {
        this.user_password = params.user_password;
    }

    if (typeof params.user_old_password !== 'undefined') {
        this.user_old_password = params.user_old_password;
    }

    if (typeof params.user_email !== 'undefined') {
        this.user_email = params.user_email;
    }

    if (typeof params.user_email_confirmation_code !== 'undefined') {
        this.user_email_confirmation_code = params.user_email_confirmation_code;
    }

    if (typeof params.user_registration_complete !== 'undefined') {
        this.user_registration_complete = params.user_registration_complete;
    }

    if (typeof params.user_email_confirmed !== 'undefined') {
        this.user_email_confirmed = params.user_email_confirmed;
    }

    if (typeof params.user_wants_highlight_emails !== 'undefined') {
        this.user_wants_highlight_emails = params.user_wants_highlight_emails;
    }
    
    this.user_first_name = '';
    if (typeof params.user_first_name !== 'undefined') {
        this.user_first_name = params.user_first_name;
    }

    this.user_last_name = '';
    if (typeof params.user_last_name !== 'undefined') {
        this.user_last_name = params.user_last_name;
    }
};

/**
 * list of usernames of DataNile
 * @param callback
 */
User.prototype.get_all_users = function (callback) {
    
    db.mysql.query('select username from user ', function (error, result) {
        callback(error, result);
    });
};

/**
 * Get the users who has confirmed his email and has permission to send Highlight emails
 */
User.prototype.getHighlightUsers = function (callback) {

    db.mysql.query("SELECT * FROM user where user_email_confirmed='Y' AND user_wants_highlight_emails='Y' ", function (error, result) {
        callback(error, result);
    });
};

User.prototype.update = function (callback) {
    var self = this;
    var response = {},
     errors = {};
    this.updateValidate(callback, function () {
        
     self.findByUserid(function (error, result) {
            if (error) {
                callback(error);
            }

      if (!mainHelper.stringCryptTest(self.user_old_password, result.user_password_hash)) {
                response.status = 'error';
                response.message = 'Password is not correct';
                errors.user_old_password = 'Please input correct password';
                response.errors = errors;
                callback(response);
            }

      self.updateData(callback);
        });
    });
};

User.prototype.completeInvitationSignUp = function (callback) {
    var self = this;
    this.updateValidate(callback, function () {
        self.updateData(callback);
    });
};

/**
 * Validate fields before update
 *
 * @param {Function} callbackError call when have errors
 * @param {Function} callbackOK call when pass validate
 */
User.prototype.updateValidate = function (callbackError, callbackOk) {
    
    var self = this;
    var index = 0;
    var callbacks = [];
    var errors = {};
    var response = {};
    var message = [];

    // Check username
    callbacks.push(function () {
        db.mysql.query('SELECT * FROM `user` WHERE `username` = ? AND `user_id` != ?',
         [self.username, self.user_id], function (err, result, fields) {
            if (err) {
                callbackError(err);
            }

           if (typeof result[0] !== 'undefined') {
                errors.username = "Username '" + self.username + "' is used.";
                message.push('Username already in use.');
            }
            
          callbacks[++index]();
        });
    });

    // Check user_email
    callbacks.push(function () {
        db.mysql.query('SELECT * FROM `user` WHERE `user_email` = ? AND `user_id` != ?',
         [self.user_email, self.user_id], function (err, result, fields) {
            if (err) {
                callbackError(err);
            }

          if (typeof result[0] !== 'undefined') {
                errors.user_email = "Email '" + self.user_email + "' is used.";
                message.push('Email already in use.');
            }
            
          callbacks[++index]();
        });
    });

    // Check if have errors
    callbacks.push(function () {
        if (Object.keys(errors).length === 0) {
            callbackOk();
        } else {
            message = message.join(' and ');
            response.status = 'error';
            response.message = message;
            response.errors = errors;
            callbackError(response);
        }
    });

    callbacks[index]();
};

User.prototype.updateData = function (callback) {
    var self = this;
    //console.log("in update Data ::+"+JSON.stringify(self));
    var obj = {};

    if (self.user_first_name !== '') {
        obj.user_first_name = self.user_first_name;
    }

    if (self.user_last_name !== '') {
        obj.user_last_name = self.user_last_name;
    }

    if (typeof self.user_email !== 'undefined') {
        obj.user_email = self.user_email;
    }

    if (typeof self.username !== 'undefined') {
        obj.username = self.username;
    }
    
    if (typeof self.user_password !== 'undefined') {
        obj.user_password_hash = mainHelper.stringCrypt(self.user_password);
    }

    if (typeof self.user_email_confirmation_code !== 'undefined') {
        obj.user_email_confirmation_code = self.user_email_confirmation_code;
    }

    if (typeof self.user_email_confirmed !== 'undefined') {
        obj.user_email_confirmed = self.user_email_confirmed;
    }

    if (typeof self.user_registration_complete !== 'undefined') {
        obj.user_registration_complete = self.user_registration_complete;
    }

    if (typeof self.user_wants_highlight_emails !== 'undefined') {
        obj.user_wants_highlight_emails = self.user_wants_highlight_emails;
    }
    
    obj.user_updated_dtime = dateHelper.toMySQL(new Date());
    
    db.mysql.query('UPDATE `user` SET ? WHERE `user_id` = ' + self.user_id,
        obj,
        function (err, result, fields) {
        callback(err, result);
    }
    );
};


/**
 * Save user in database
 *
 * @param {Function} callback function, that will call
 */
User.prototype.save = function (callback) {

    if (this.user_id === null) {
        this.insert(callback);
    } else {
        if (this.user_email_confirmation_code === undefined) {
            this.update(callback);
        } else {
            this.compliteRegister(callback);
        }

    }
};


/**
 * Method for insert new user.
 *
 * @param {Function}
 *            callback callback function with result
 */
User.prototype.insert = function (callback) {
    var self = this;
    this.insertValidate(
        callback,
        function () {
            if (typeof self.user_password !== 'undefined') {
                self.user_password = mainHelper.stringCrypt(self.user_password);
            }

         var user_email_confirmation_code = mainHelper.randomToken();
            db.mysql
              .query(
                'INSERT INTO `user` SET ?',
                {
                email: self.user_email,
                password_hash: self.user_password,
                email_confirmation_code: user_email_confirmation_code,
                created_dtime: dateHelper.toMySQL(new Date()),
                updated_dtime: dateHelper.toMySQL(new Date()),
                first_name: self.user_first_name,
                last_name: self.user_last_name,
                registration_complete: 0
            },
                function (err, result, fields) {
                    if (!err) {
                        self.user_id = result.insertId;
                        self.user_email_confirmation_code = user_email_confirmation_code;
                    }

                 callback(err, result);
                });
        });

};

/**
 * Validate fields
 *
 * @param {Function}
 *            callbackError call when have errors
 * @param {Function}
 *            callbackOK call when pass validate
 */
User.prototype.insertValidate = function (callbackError, callbackOK) {
    var self = this;
    var index = 0;
    var callbacks = [];
    var errors = {};
    var response = {};
    // Check username
    
    // Check user_email
    callbacks.push(function () {
        db.mysql.query('SELECT * FROM `user` WHERE `email` = ?',
          [self.user_email], function (err, result, fields) {
            if (err) {
                callbackError(err);
            }
            
           if (typeof result[0] !== 'undefined') {
                errors.user_email = "Email '" + self.user_email + "' is used.";
            }
            
           callbacks[++index]();
        });
    });

    // Check if have errors
    callbacks.push(function () {
        if (Object.keys(errors).length === 0) {
            callbackOK();
        } else {
            response.status = 'error';
            response.message = 'Some of the details were already in use.';
            response.errors = errors;
            callbackError(response);
        }
    });

    callbacks[index]();
};

User.prototype.compliteRegister = function (callback) {
    var obj = {
        user_email_confirmed: 'Y',
        user_registration_complete: 'Y',
        user_updated_dtime: dateHelper.toMySQL(new Date())
    };
    var self = this;
    if (typeof this.user_password !== 'undefined') {
        obj.user_password_hash = mainHelper.stringCrypt(this.user_password);
    }

    if (typeof this.username !== 'undefined') {
        obj.username = this.username;
    }

    if (this.user_first_name !== '') {
        obj.user_first_name = this.user_first_name;
    }

    if (this.user_last_name !== '') {
        obj.user_last_name = this.user_last_name;
    }

    db.mysql.query("UPDATE `user` SET ? WHERE `user_email` = '"
            + this.user_email + "' AND `user_email_confirmation_code` = '"
            + this.user_email_confirmation_code + "'", obj,
        function (err, result, fields) {
        callback(err, result);
    }
    );
};

User.prototype.loginWithEmail = function (callback) {
    db.mysql.query('SELECT * FROM `user` WHERE `user_email` = ? AND `user_email_confirmation_code` = ?',
        [this.user_email, this.user_email_confirmation_code],
        function (err, result, fields) {
            if (typeof result[0] === 'undefined') {
                err = new Error(
                  'Invalid user_email or code. Please try again.');
            } else {
                result[0] = showUserResult(result[0]);
                result[0].stoken = mainHelper.randomToken();
                memcache.set(result[0].stoken, JSON.stringify({
                    user_id: result[0].user_id
                }));
            }
            
         callback(err, result[0]);
        });
};

User.prototype.login = function (callback) {
    var self = this;

    db.mysql.query('SELECT * FROM `user` WHERE `username` = ?',
        [this.username],
        function (err, result, fields) {

        console.log('result in login ::' + JSON.stringify(result));
        
        if (typeof result[0] === 'undefined') {
            err = new Error('Invalid username or password. Please try again.');
        }else if (result[0].user_email_confirmed != 'Y') {
            err = new Error('An email confirmation from DataNile was sent to your address. Please check your email and confirm in order to activate your account.');
        }else if (!mainHelper.stringCryptTest(self.user_password, result[0].user_password_hash)) {
            err = new Error('Invalid username or password. Please try again.');
        } else {
            result[0] = showUserResult(result[0]);
            result[0].stoken = mainHelper.randomToken();
            memcache.set(result[0].stoken, JSON.stringify({
                user_id: result[0].user_id
            }));

        }
        
        callback(err, result[0]);
    });
};

/**
 * Get user information based on one or more fields
 * Fields : user_email, username, user_email_confirmation_code, user_id
 */

User.prototype.getInfo = function (callback) {
    var self = this;
    var where = [];
    var obj = [];
    if (typeof self.user_email !== 'undefined') {
        where.push('u.user_email = ?');
        obj.push(self.user_email);
    }
    
    if (typeof self.username !== 'undefined') {
        where.push('u.username = ?');
        obj.push(self.username);
    }
    
    if (typeof self.user_email_confirmation_code !== 'undefined') {
        where.push('u.user_email_confirmation_code = ?');
        obj.push(self.user_email_confirmation_code);
    }

    if (self.user_id !== null) {
        where.push('u.user_id = ?');
        obj.push(self.user_id);
    }

    where = ' WHERE ' + where.join(' AND ');

    //console.log(self.user_id+"wheree inuse ::"+where);

    db.mysql.query('SELECT COUNT(DISTINCT ds.data_set_id) as data_sets, COUNT(DISTINCT m.mashup_id) as mashups, u.* FROM user u ' +
            'LEFT JOIN vault_to_user v2u on v2u.user_id = u.user_id ' +
            'LEFT JOIN vault v on v.vault_id = v2u.vault_id ' +
            'LEFT JOIN data_set ds on ds.vault_id = v.vault_id ' +
            'LEFT JOIN mashup m on m.vault_id = v.vault_id ' + where + ' ',
            obj, function (err, result, fields) {
        
                //console.log(" result:"+JSON.stringify(result));

                if (typeof result !== 'undefined') {
            if (typeof result[0] !== 'undefined') {
                result[0] = showUserResult(result[0]);
                callback(err, result[0]);
            } else {
                callback(err, result);
            }
        }else {
            callback(err, result);
        }

            });
};

/**
 * Find by email
 *
 * @param {Function}
 *            callback [description]
 */
User.prototype.findByEmail = function (callback) {
    var self = this;
    db.mysql.query('SELECT * FROM `user` WHERE `user_email` = ?',
      [self.user_email], function (err, result, fields) {
        callback(err, result[0]);
    });
};

/**
 * Find by username
 *
 * @param {Function}
 *            callback [description]
 */
User.prototype.findByUsername = function (callback) {
    var self = this;
    db.mysql.query('SELECT * FROM `user` WHERE `username` = ?',
      [self.username], function (err, result, fields) {
        callback(err, result[0]);
    });
};

/**
 * Find by user_id
 *
 * @param {Function}
 *            callback [description]
 */
User.prototype.findByUserid = function (callback) {
    var self = this;
    db.mysql.query('SELECT * FROM `user` WHERE `user_id` = ?',
      [self.user_id], function (err, result, fields) {
        callback(err, result[0]);
    });
};

/**
 * Find by email confirmation
 *
 * @param {Function}
 *            callback
 */
User.prototype.findByConfirmationCode = function (callback) {
    var self = this;
    db.mysql.query(
      'SELECT * FROM `user` WHERE `user_email_confirmation_code` = ?',
      [self.user_email_confirmation_code],
      function (err, result, fields) {
        callback(err, result[0]);
    });
};

/**
 * Delete fields
 *
 * @param {Object}
 *            result User info
 * @return {Object}
 */
function showUserResult(result) {
    delete result.user_password_hash;
    delete result.user_email_confirmed;
    delete result.user_email_confirmation_code;
    return result;
}

exports.User = User;
