/**
 * Table `UserToUser`
 */

var  db = require("../lib/Database");

/**
 * Constructor
 * 
 * @param {Object}
 *            params
 */
var UserToUser = function(params) {
	
	if (typeof params === "undefined") {
		params = {};
	}
	 
	if (typeof params.user_id !== 'undefined') {
		this.user_id = params.user_id;
	}

	if (typeof params.other_user_id !== 'undefined') {
		this.other_user_id = params.other_user_id;
	}

	if (typeof params.u2u_stop_highlight_emails !== 'undefined') {
		this.u2u_stop_highlight_emails = params.u2u_stop_highlight_emails;
	}

	 
};

UserToUser.prototype.getAlertStatus = function(callback){
 
	db.mysql.query("SELECT * FROM user_to_user where user_id = ? AND other_user_id = ? ", [ this.user_id, this.other_user_id ], function(error, result) {
		callback(error, result);
	});
	 
};


UserToUser.prototype.updateAlertStatus = function(callback){
	
	var self = this;
	
	db.mysql.query("SELECT * FROM user_to_user WHERE user_id = ? AND other_user_id = ? ", [ this.user_id, this.other_user_id ], function(error, result) {
		
		if(result.length == 0 ){
			
			db.mysql.query("INSERT INTO user_to_user SET ? ",
					{
						u2u_stop_highlight_emails : self.u2u_stop_highlight_emails , 
						user_id : self.user_id, 
						other_user_id :	self.other_user_id 
					
					},
					function(error, result) {
							callback(error, result);
			});
			
		}else{
			
			db.mysql.query("UPDATE user_to_user SET u2u_stop_highlight_emails = ? WHERE user_id = ? AND other_user_id = ? ", [ self.u2u_stop_highlight_emails, self.user_id, self.other_user_id ], function(error, result) {
				callback(error, result);
			});
		}
		
		
	});
	
	
	 
};


exports.UserToUser = UserToUser;