/**
 * Account controller. Sign in, sign out, forgot password etc.
 */
var User = require('../tables/User').User,
	memcache = require('../lib/Memcache').mcClient,
	smtp = require('../lib/Smtp').transport,
	templateHelper = require('../helpers/TemplateHelper'),
	mainHelper = require('../helpers/MainHelper'),
	config = require('../config'),
	UserToUser = require('../tables/User_To_User').UserToUser;

/**
 * Check user authentication
 *
 */
this.authenticate = function (req, res, next) {
    var stoken = req.param('stoken');
    memcache.get(stoken, function (error, result) {
        if (error == 'NOT_FOUND' || result === null) {
            error = new Error('You need to be signed in to access this resource.');
        }

     if (error) {
            next(error);
        } else {
            req.body.auth_user_id = JSON.parse(result).user_id;
      /*if (typeof req.body.user_id === 'undefined') {
       req.body.user_id = JSON.parse(result).user_id;
      } else {
       req.body.auth_user_id = JSON.parse(result).user_id;
      }*/
            next();
        }
    });
};

/**
 * User sign in
 *
 */
this.signIn = function (req, res, next) {
    var user = new User(req.body);
    user.login(function (error, result) {
        if (error) {
            next(error);
        } else {
            res.send({
                status: 'ok',
                message: 'Signed in.',
                user: result,
                stoken: result.stoken
            });
        }
    });
};

/**
 * User Registration
 *
 */
this.signUp = function (req, res, next) {
    var user = new User(req.body);
    user.save(function (error, result) {
        if (user.user_id === null) {
            next(error);
        } else {
            html = templateHelper.getHtml('email/registration_confirmation.html',
              {
                link: config.links.registrationConfirm + '/' + user.user_id + '/' + user.user_email + '/' + user.user_email_confirmation_code,
                username: user.username
            }
            );
            smtp.sendMail(
              {
                from: config.links.support,
                to: user.user_email,
                subject: config.messages.registrationMailSubject,
                html: html,
                generateTextFromHTML: true
            },
              function (error, result) {
                console.log(error);
                console.log(result);
                if (error) {
                    next(new Error(error));
                    console.log('::-- Sending email error thrown --::' + JSON.stringify(error));
                }
                
                console.log('Sending email to ::' + user.user_email);
                console.log('Sending email result ::' + JSON.stringify(result));
                console.log('Sending email error ::' + JSON.stringify(error));
            }
            );
            res.send({
                status: 'ok',
                message: 'Your account has been created. Please check your mail to activate your account.'
            });
        }
    });
};

/**
 * User Registration Confirm
 */

this.confirm = function (req, res, next) {
    var user = new User(req.params);
    user.save(function (error, result) {
        if (error) {
            next(error);
        }else {
            res.send({
                status:  'ok',
                message: 'Your account has been activated.'
            });
        }
    });
};

/**
 * Check whether registration completed or not
 */

this.checkRegistration = function (req, res, next) {
    var user = new User(req.body);
    user.getInfo(function (error, result) {
        if (error) {
            next(error);
        }else {
            res.send({
                status:  'ok',
                rec: result
            });
        }
    });
};

/**
 * User update : Invitation sign up
 */
this.completeInvitation = function (req, res, next) {
    var user = new User(req.body);
    user.completeInvitationSignUp(function (error, result) {
        if (error) {
            next(error);
        } else {
            user.getInfo(function (error, result) {
                if (error) {
                    next(error);
                } else {
                    res.send({
                     status:  'ok',
                     message: 'Your information has been updated.',
                     rec: result
                    });
                }
            });
        }
    });
};

/**
 * User update : Account update
 */
this.update = function (req, res, next) {
    var user = new User(req.body);
    user.update(function (error, result) {
        if (error) {
            next(error);
        } else {
            user.getInfo(function (error, result) {
                if (error) {
                    next(error);
                } else {
                    res.send({
                     status:  'ok',
                     message: 'Your information has been updated.',
                     rec: result
                    });
                }
            });
        }
    });
};

/**
 * Get user information
 *
 */
this.getInfo = function (req, res, next) {
    
	var user = new User(req.query);
    
	user.getInfo(function (error, result) {
        if (error) {
            next(error);
        } else {

         res.send({
                status:  'ok',
                rec: result
            });
        }
    });
};

/**
 * User sign out
 *
 */
this.signOut = function (req, res, next) {
    var stoken = req.param('stoken');
    memcache.delete(stoken, function (error, result) {
        res.send({
            status: 'ok',
            message: 'Signed out.'
        });
    });
};

/**
 * If user forgot password
 *
 */
this.forgotPassword = function (req, res, next) {

	// Initialise User with req body (username or email )
    var user = new User(req.body);
    
	// New Confirmation code
    var code = mainHelper.randomToken();

	// send mail
    var sendEmail = function (userRec) {

     var html = templateHelper.getHtml('email/password_reset.html', { link: config.links.PasswordReset + '/' + userRec.user_id + '/' + code });
        smtp.sendMail(
          {
            from: config.links.support,
            to: userRec.user_email,
            subject: 'Password Reset',
            html: html,
            generateTextFromHTML: true
        },
          function (error, result) {
            if (error) {
                next(new Error(error));
            }else {
                res.send({
                    status: 'ok',
                    message: 'Please check your email.'
                });
            }
        }
        );
        
    };
    
	//Update record with new Confirmation code
    var UpdateRecord = function (userRec) {
        var userM = new User({
            user_id: userRec.user_id,
            user_email_confirmation_code: code
        });
        userM.updateData(function (error, result) {
            if (error) {
                next(error);
            } else {
                sendEmail(userRec);
            }
        });
    };

	// check is there any user with this email or username
    user.getInfo(function (error, result) {
        if (!error && typeof result === 'undefined') {
            error = new Error('User with this email or username has not found.');
        }

     if (error) {
            next(error);
        } else {
            UpdateRecord(result);
        }
    });
};

/**
 * User reset password : we will set new password and send it to user
 */

this.resetPassword = function (req, res, next) {
    
	var user = new User(req.query);

	var UpdateRecord = function (userRec) {
        
		// New password
        var newPwd = mainHelper.randomNumber();
        
		// Update record with new password
        var userM = new User({
            user_id: userRec.user_id,
            user_password: newPwd
        });
        userM.updateData(function (error, result) {
            if (error) {
                next(error);
            } else {
                userRec.password = newPwd;
                res.send({
                    status: 'ok',
                    message: 'Password has been reset.',
                    rec: userRec
                });
            }
        });
    };

	// check is there any user with this code and user_id
    user.getInfo(function (error, result) {
        if (!error && typeof result === 'undefined') {
            error = new Error('corfirmation code is not correct or no user found with given details.');
        }
        
     if (error) {
            next(error);
        } else {
            UpdateRecord(result);
        }
    });
    
};

this.get_all_users = function (req, res, next) {
    
	var user = new User();
    user.get_all_users(function (error, users) {

     if (error) {
            next(error);
        }else {
            
      res.send({
                status: 'ok',
                recs: users
            });
        }
    });

};

this.alertStatus = function (req, res, next) {
    
 	var user_to_user = new UserToUser(req.query);
    
	user_to_user.getAlertStatus(function (error, result) {
        
		if (error) {
            next(error);
        }else {
            
			res.send({
                status: 'ok',
                rec: result
            });
        }
    });

};

this.updateAlertStatus = function (req, res, next) {
    
	var user_to_user = new UserToUser(req.body);
    
	user_to_user.updateAlertStatus(function (error, result) {
        
		if (error) {
            next(error);
        }else {

			res.send({
                status: 'ok',
                message: 'Alerts status updated successfully'
            });
        }
    });

};
