/**
 * Route : Account Description : contains routes regarding user User
 * account(sign up, sign in, edit). Author : Ashok k <ashok.k@logictreeit.com>
 */

var account = require('../controllers/Account'), 
	validateHelper = require("../helpers/ValidateHelper");

module.exports = function(app) {
	
	app.post("/account/sign_in", function(req, res, next) {
	
		req.assert("username", 'Please input username.').notEmpty();
		req.assert("user_password", 'Please input password.').notEmpty();

		var errors = validateHelper.makeErrors(req.validationErrors());

		if (Object.keys(errors).length > 0) {
			var response = {};
			response.status = "error";
			response.message = "Please input required details. ";
			response.errors = errors;
			next(response);
			return;
		}
		req.sanitize("username").xss();
		next();
	}, account.signIn);

	app.post("/account/register", function(req, res, next) {

		req.assert("mobile_number", 'Please input mobile number.').notEmpty();
		req.assert("user_password", 'Please input password.').notEmpty();
		req.assert("confirm_password", 'Please input confirm password.').notEmpty();
		if (typeof req.param("user_password") !== "undefined") {
			req.assert("confirm_password", "Passwords are not same.").equals(
					req.param("user_password"));
		}
		req.assert("user_email", 'Please input email.').notEmpty();
		
		var errors = validateHelper.makeErrors(req.validationErrors());
		
		if (Object.keys(errors).length > 0) {
			var response = {};
			response.status = "error";
			response.message = "Please input required details. ";
			response.errors = errors;
			next(response);
			return;
		}
		req.sanitize("mobile_number").xss();
		if (typeof req.param("user_first_name") !== "undefined") {
			req.sanitize("user_first_name").xss();
		}
		if (typeof req.param("user_last_name") !== "undefined") {
			req.sanitize("user_last_name").xss();
		}
		if (typeof req.param("user_email_confirmation_code") !== "undefined") {
			req.sanitize("user_email_confirmation_code").xss();
		}

		next();
	}, account.signUp);

	// Invitations sign up 
	app.put("/account/update", function(req, res, next) {

		req.assert("username", 'Please input username.').notEmpty();
		req.assert("user_password", 'Please input password.').notEmpty();
		req.assert("confirm_password", 'Please input confirm password.').notEmpty();
		if (typeof req.param("user_password") !== "undefined") {
			req.assert("confirm_password", "Passwords are not same.").equals(
					req.param("user_password"));
		}
		req.assert("user_email", 'Please input email.').notEmpty();
		req.assert("user_email", 'Email is not correct.').isEmail();
		
		var errors = validateHelper.makeErrors(req.validationErrors());
		
		if (Object.keys(errors).length > 0) {
			var response = {};
			response.status = "error";
			response.message = "Please input required details. ";
			response.errors = errors;
			next(response);
			return;
		}
		req.sanitize("username").xss();
		if (typeof req.param("user_first_name") !== "undefined") {
			req.sanitize("user_first_name").xss();
		}
		if (typeof req.param("user_last_name") !== "undefined") {
			req.sanitize("user_last_name").xss();
		}
		if (typeof req.param("user_email_confirmation_code") !== "undefined") {
			req.sanitize("user_email_confirmation_code").xss();
		}

		next();
	}, account.completeInvitation );
	
	// Account update
	app.put("/account/edit_user",
			account.authenticate,
			function(req, res, next) {
				req.assert("user_old_password",'Please input password.').notEmpty();

				req.assert("user_password", 'Please input password.').notEmpty();
				req.assert("confirm_password", 'Please input confirm password.').notEmpty();
				
				if (typeof req.param("user_password") !== "undefined" && req.param("user_password") !== "" && req.param("confirm_password") !== "") {
					req.assert("confirm_password", "Passwords are not same.").equals(req.param("user_password"));
				}
				
				req.assert("user_email", 'Please input Email.').notEmpty();
				
				if (typeof req.param("user_email") !== "undefined" && req.param("user_email") !== "") {
					req.assert("user_email", 'Email is not correct.').isEmail();
					req.sanitize("user_email").xss();
				}
				
				var errors = validateHelper.makeErrors(req.validationErrors());

				if (typeof req.param("user_password") !== "undefined" && req.param("user_password") !== "" && req.param("user_old_password") !== "") {
					if(req.param("user_password") === req.param("user_old_password")){
						errors.user_password = "Don't re use current password, try new.";
					}
				}
				
				if (Object.keys(errors).length > 0) {
					var response = {};
					response.status = "error";
					response.message = "Please input required details. ";
					response.errors = errors;
					next(response);
					return;
				}
				
				req.sanitize("user_old_password").xss();
				if (typeof req.param("user_first_name") !== "undefined") {
					req.sanitize("user_first_name").xss();
				}
				if (typeof req.param("user_last_name") !== "undefined") {
					req.sanitize("user_last_name").xss();
				}
				next();
			}, account.update);
	
	// Registration confirmation
	app.post("/account/confirm/:user_id/:user_email/:user_email_confirmation_code",
					function(req, res, next) {
						next();
					}, account.confirm);

	
	app.post("/account/sign_out", account.signOut);


	app.get("/account", account.authenticate, account.getInfo);
	
	// Check whether user is registration completed or not
	app.post("/account/is_completed", function(req, res, next) {
		
		req.assert("user_email", 'Please input email.').notEmpty();
		req.assert("user_email", 'Email is not correct.').isEmail();
		
		var errors = validateHelper.makeErrors(req.validationErrors());
		
		if (Object.keys(errors).length > 0) {
			var response = {};
			response.status = "error";
			response.message = "Invalid details(email), Unable to validate Account !";
			response.errors = errors;
			next(response);
			return;
		}
		next();
	}, account.checkRegistration);
	
	// sending email with a link to reset password
	app.post("/account/forgot", function(req, res, next) {

		if (typeof req.param("username") !== "undefined") {
			req.assert("username", 'Please input Username.').notEmpty();
		}else {
			req.assert("user_email", 'Please input Email.').notEmpty();
			
			if (typeof req.param("user_email") !== "undefined" && req.param("user_email") !== "") {
				req.assert("user_email", 'Email is not correct.').isEmail();
				req.sanitize("user_email").xss();
			}
		}
		
		var errors = validateHelper.makeErrors(req.validationErrors());
		
		if (Object.keys(errors).length > 0) {
			var response = {};
			response.status = "error";
			response.message = "Please input required details.";
			response.errors = errors;
			next(response);
			return;
		}
		
		if (typeof req.param("user_email") !== "undefined") {
			req.sanitize("user_email").xss();
		}
		if (typeof req.param("username") !== "undefined") {
			req.sanitize("username").xss();
		}
		next();
	}, account.forgotPassword);

	// reset the password and display username and password 
	app.get("/account/reset-password", function(req, res, next) {
		
		req.assert("code", 'Code is not correct.').notEmpty();
		req.assert("user_id", 'invalid user id.').notEmpty();
		
		var errors = validateHelper.makeErrors(req.validationErrors());
		
		if (Object.keys(errors).length > 0) {
			var response = {};
			response.status = "error";
			response.message = "Invalid details(email/code), Unable to reset your password!";
			response.errors = errors;
			next(response);
			return;
		}
		req.sanitize("code").xss();
		next();
	}, account.resetPassword);
	
	app.get("/users", account.authenticate, account.get_all_users);
	
	
	// Get alerts status 
	app.get("/user/block-alerts-status", account.authenticate, function(req, res, next) {
		
		console.log(" in router");
		
		req.assert("user_id", 'Please input user_id.').notEmpty();
		req.assert("other_user_id", 'Please input other user id.').notEmpty();
		
		var errors = validateHelper.makeErrors(req.validationErrors());
		
		if (Object.keys(errors).length > 0) {
			var response = {};
			response.status = "error";
			response.message = "Please input required details.";
			response.errors = errors;
			next(response);
			return;
		}
 		next();
 		
	}, account.alertStatus);
	
	
	// Get alerts status 
	app.post("/user/update-alerts-status", account.authenticate, function(req, res, next) {
		
		req.assert("user_id", 'Please input user_id.').notEmpty();
		req.assert("other_user_id", 'Please input other user id.').notEmpty();
		req.assert("u2u_stop_highlight_emails", 'Please input other u2u_stop_highlight_emils.').notEmpty();
		
		var errors = validateHelper.makeErrors(req.validationErrors());
		
		if (Object.keys(errors).length > 0) {
			var response = {};
			response.status = "error";
			response.message = "Please input required details.";
			response.errors = errors;
			next(response);
			return;
		}
 		next();
 		
	}, account.updateAlertStatus);
};