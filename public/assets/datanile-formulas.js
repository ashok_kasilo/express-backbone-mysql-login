 
fr_function_list['average'] =  
{	
		  'parameters' : [ {'param_name' : 'values', 'param_type' : 'number', 'take_additional_params_as_list' : 'Y'  } ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_avg }
};

fr_function_list['minimum'] =  
{
		  'parameters' : [ {'param_name' : 'values', 'param_type' : 'number', 'take_additional_params_as_list' : 'Y'  } ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_min }
};

fr_function_list['maximum'] =  
{
		  'parameters' : [ {'param_name' : 'values', 'param_type' : 'number', 'take_additional_params_as_list' : 'Y'  } ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_max }
}; 
fr_function_list['multiplication'] =  
{
		  'parameters' : [ {'param_name' : 'values', 'param_type' : 'number', 'take_additional_params_as_list' : 'Y'  } ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_mul }
}; 
fr_function_list['division'] =  
{ 
		  'parameters' : [ {'param_name' : 'n', 'param_type' : 'number' }, {'param_name' : 'n1', 'param_type' : 'number' }, ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_div }
}; 
fr_function_list['subtraction'] =  
{
	    'parameters' : [ {'param_name' : 'values', 'param_type' : 'number', 'take_additional_params_as_list' : 'Y'  } ], 
	    'func' : fr_calc_easy_func ,
	    'func_settings' : { 'helper_func' : fr_calc_easy_sub }
}; 
fr_function_list['data_point'] =  
{
		  'parameters' : [ {'param_name' : 'data_set_name', 'param_type' : 'string' } , {'param_name' : 'vault_name', 'param_type' : 'string'} , {'param_name' : 'label_index', 'param_type' : 'function' } ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_data_point }
}; 
/*fr_function_list['calculation'] =  
{
		  'parameters' : [ {'param_name' : 'calculation_name', 'param_type' : 'string' } , {'param_name' : 'vault_name', 'param_type' : 'string'} ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_calculation }
}; */
fr_function_list['calculation'] =  
{
		  'parameters' : [ {'param_name' : 'calculation_name', 'param_type' : 'string' } , {'param_name' : 'vault_name', 'param_type' : 'string'},  {'param_name' : 'label_value', 'param_type' : 'function' } ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_calculation_on_current_label }
};
fr_function_list['current_label'] =  
{
		  'parameters' : [ {'param_name' : 'label_index', 'param_type' : 'number' }  ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_current_label_set }
}; 
fr_function_list['percentage_growth'] =  
{ 
		  'parameters' : [ {'param_name' : 'param_1', 'param_type' : 'function' }, {'param_name' : 'param_2', 'param_type' : 'function' }, ], 
		  'func' : fr_calc_easy_func ,
		  'func_settings' : { 'helper_func' : fr_calc_easy_percentage_growth }
};


//a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_avg(args){

	var ret = 0,i=0;
	for(var n = 0; n < args.values.length; n++){
      
		ret += args.values[n];
	  	i++;
   }

   return (ret/i).toFixed(6);
}

//a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_min(args){

	var min = args.values[0];
	for(var n = 0; n < args.values.length; n++){
		if( min > args.values[n] ) min = args.values[n];
	}

	return min;
}

//a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_max(args){

	var max = args.values[0];
	for(var n = 0; n < args.values.length; n++){
		if( max < args.values[n] ) max = args.values[n];
	}

	return max;
}

//a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_mul(args){

	var ret = 1;
   for(var n = 0; n < args.values.length; n++){
      ret *= args.values[n];
   }
   return ret;
}

//a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_div(args){

	return (args['n'] / args['n1']).toFixed(6) ;
}

//a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_sub(args){

	var ret = args.values[0];
   for(var n = 1; n < args.values.length; n++){
      ret -= args.values[n];
   }
   return ret;
}

//set sample label value in Mashup edit view table
function set_sample_label_value(mashup_id, HtmlElementId){
	
	var label_id = get_sample_label_id(mashup_id);
	
	var label = new Label({ label_id : label_id });
	label.fetch({ async : false });
	
	$(HtmlElementId).append(label.get("label_value"));
	
	CURRENT_LABEL_ID = label_id;
	SAMPLE_LABEL_ID = label_id;
	console.log("in set sample label "+CURRENT_LABEL_ID);
}

var CURRENT_LABEL_ID, SAMPLE_LABEL_ID;

function fr_calc_easy_current_label_set(args){
	
	var label_index = parseInt(args["label_index"]);
	
	if(label_index != 0 ){
		
		console.log("CURRENT_LABEL_ID :"+CURRENT_LABEL_ID);
		  var current_label_model = app.label_collection.where({ label_id : CURRENT_LABEL_ID });
		  var current_label_model_index = app.label_collection.indexOf(current_label_model[0]);
		  
		  var _index;
		  
		  if(label_index == -1){
			  _index = current_label_model_index - 1;
		  }else if(label_index == -12){
			  _index = current_label_model_index - 12;
		  }
		  
		  if(!_.isUndefined(app.label_collection.at(_index))){
			  
			  var _new_label_model = app.label_collection.at(_index);
			  CURRENT_LABEL_ID = _new_label_model.get("label_id");
			  
		  }
		  //console.log("in current label_id::"+CURRENT_LABEL_ID+" :: label_index ::"+label_index+ " :: SAMPLE_LABEL_ID ::"+SAMPLE_LABEL_ID);
	}
	//console.log("in current label_id::"+CURRENT_LABEL_ID+" :: label_index ::"+label_index);
	return CURRENT_LABEL_ID;
	
}

// get the sample label id of all data sets in the current mashup
function get_sample_label_id(mashup_id){
	
	 var _label_id = 0;
	 
	 Backbone.ajax({
		 
		  type : "GET",
		  url : BASEURL_API + "label/get_sample_label" ,
		  async : false,
		  data : {
			  "mashup_id" :  mashup_id
		  },
	
		  success : function(response) {
	
			  if (response.status === "ok") {
				  
				  if(!_.isUndefined(response.recs[0])){
					  _label_id = response.recs[0].label_id;  
				  }else{
					  _label_id = 0;
				  }
				  
			  }
			  if (response.status === "error") {
				  _label_id  = 0;
			  }
		  },
	 });
	 
	 return _label_id;
}
 
function fr_calc_easy_data_point(args){
	
	/*var data_point = 0 , label_id = args["label_index"];
	// get the data point from DB with 3 param
	Backbone.ajax({
		type : "GET",
		url : BASEURL_API + "datapoint/get_data_point" ,
		async : false,
		data : {
			"data_set_name" :args["data_set_name"],
			"vault_name" : args["vault_name"],
			"label_id" : label_id 
		},
	
		success : function(response) {
			if (response.status === "ok") {
				 
				if(!_.isUndefined(response.recs[0])){

					if(response.recs[0].dp_value != ""){
						data_point = parseFloat(response.recs[0].dp_value);
					}

				}
				
			}
			if (response.status === "error") {
				data_point = 0;
			}
			
		}
	});
	return data_point;*/
	var data_point = 0;
		
	var dps_array = app.dp_value_collection.where({ ds_name : args["data_set_name"], vault_name : args["vault_name"], label_id : args["label_index"] });
	
	if(dps_array.length > 0 ){
		
		data_point = dps_array[0].get("dp_value");
		
		data_point = prase_datapoint_for_chart(data_point);
		
		data_point = parseFloat(data_point);
		
	}else{
		data_point = 0;
	}
		
	return data_point;
}

function fr_calc_easy_calculation_on_current_label(args){
	
	//console.log(args["calculation_name"]+ " - " +"app.mashupItems:"+JSON.stringify(app.mashupItems));
	
	var m2i_model = app.mashupItems.where({ m2i_calculation_name : args["calculation_name"] });
	
	if(m2i_model.length == 0){
		return 0.0;
	}
	 
	var formula_string = m2i_model[0].get("m2i_formula");
	
	//console.log("FUNC :"+fr_parse_program(formula_string).left.left.children.length);
	//console.log("FUNC PARAM Parse:"+JSON.stringify(fr_parse_program(formula_string)));

	var calculation_value = fr_run_code(fr_parse_program( formula_string ) );
	
	return calculation_value;
}


function fr_calc_easy_percentage_growth(args){


	var _param_1 = args["param_1"],
		_param_2 = args["param_2"];
	
	 console.log(_param_1 + ":: percentage growth ::"+_param_2);
	 if(_param_1 == 0 || _param_2 == 0){
		 return null;
	 }
	 
	 CURRENT_LABEL_ID = SAMPLE_LABEL_ID;
	 
	/* console.log("CURRENT_LABEL_ID 1 ::"+CURRENT_LABEL_ID);
	 console.log("SAMPLE_LABEL_ID 2::"+SAMPLE_LABEL_ID);*/
	 
	 var result = 0;
	 
	 //var _calculation = ( (_param_1 / _param_2 ) - 1 ) * 100 ;
	 
	 // will apply percentage in do_formatting : which is called while displaying values 
	 var _calculation =  (_param_1 / _param_2 ) - 1  ;
	 
	 if( _.isNaN(_calculation) || !_.isFinite(_calculation) ){
		 
		 result = NaN ;
		 
	 }else{
		 
		 result = _calculation.toFixed(4);
		 //result = _calculation;
		 console.log("Growth funtion ::"+result);
		 
	 }
	 return result ;
	
} 

//this function rounds and returns the decimals to particular count that we pass as an argument(decimal_count)
function formatDecimals(value, decimals_count) {
	   
	if(decimals_count >= 0 && decimals_count <= 20){
		return value.toFixed(decimals_count);
	} else if(decimals_count <= 0) {
		return value.toFixed(0);
	} else {
		return value;
	}
	
}


