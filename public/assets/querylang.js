// returns what it expects next or a tree.

// querys stands for query stream. the S is for Stream.
// querys = { 'str' : query_string, 'query_index' : 0 }

function query_run(querys) {
    query = {};
    query.sections = [];

    if (!querys.query_index) {
        querys.query_index = 0;
    }

    while (querys.query_index < querys.str.length) {
        query_whitespace(querys);

        if (query_open_bracket_look(querys)) {
            var response = query_bracket(querys);
            if (response.error_message) {
                return response;
            }
            query.sections.push(response);
        }

        query_whitespace(querys);

        var res = query_keyword(querys, just_look = 1)
        if (!res.error_message) {
            var response = query_keyword(querys, just_look = 0);
            if (response.error_message) {
                return response;
            }
            query.autocomplete = query_keyword_autocomplete_lookup(response.keyword);
            query.sections.push(response);
        }
    }

    return query;
}

function query_whitespace(querys) {
    var found_whitespace = 0;
    while (
    querys.str[querys.query_index] == ' ' || querys.str[querys.query_index] == '\t' || querys.str[querys.query_index] == '\n' || querys.str[querys.query_index] == '\r') {
        querys.query_index++;
        found_whitespace = 1;
    }
    return found_whitespace;
}


function query_keyword(querys, just_look) {
    if (querys.query_index >= querys.str.length) return {
        "error_message": "End of query",
        "error_index": querys.query_index
    };
    if (
    querys.str.length >= querys.query_index && querys.str[querys.query_index] != ' ' && querys.str[querys.query_index] != '\t' && querys.str[querys.query_index] != '\n' && querys.str[querys.query_index] != '\r' && querys.str[querys.query_index] != '[' && querys.str[querys.query_index] != ']') {
        // this is a good normal keyword.
        if (just_look) return {};
    } else {
        // this is a weird character.
        return {
            "error_message": "You cannot start a keyword with " + querys.str[querys.query_index],
            "error_index": querys.query_index
        };
    }

    start_index = querys.query_index;
    while (
    querys.str.length >= querys.query_index && querys.str[querys.query_index] != ' ' && querys.str[querys.query_index] != '\t' && querys.str[querys.query_index] != '\n' && querys.str[querys.query_index] != '\r' && querys.str[querys.query_index] != '[' && querys.str[querys.query_index] != ']') {
        querys.query_index++;
    }
    keyword = querys.str.substr(start_index, querys.query_index - start_index);

    var ret = {};
    ret.keyword = keyword;
    return ret;
}


function query_identifier(querys, just_look) {
    if (querys.query_index >= querys.str.length) return {
        "error_message": "End of query",
        "error_index": querys.query_index
    };
    if (
    querys.str[querys.query_index] >= 'a' && querys.str[querys.query_index] <= 'z' || querys.str[querys.query_index] >= 'A' && querys.str[querys.query_index] <= 'Z' || querys.str[querys.query_index] >= '0' && querys.str[querys.query_index] <= '9') {
        // this is a good normal keyword.
        if (just_look) return {};
    } else {
        // this is a weird character.
        return {
            "error_message": "You cannot start a keyword with " + querys.str[querys.query_index],
            "error_index": querys.query_index
        };
    }

    start_index = querys.query_index;
    while (
    querys.str[querys.query_index] >= 'a' && querys.str[querys.query_index] <= 'z' || querys.str[querys.query_index] >= 'A' && querys.str[querys.query_index] <= 'Z' || querys.str[querys.query_index] >= '0' && querys.str[querys.query_index] <= '9') {
        querys.query_index++;
    }
    keyword = querys.str.substr(start_index, querys.query_index - start_index);

    var ret = {};
    ret.keyword = keyword;
    return ret;
}

function query_get_bracket_param(querys) {
    var start_index = querys.query_index;
    while (querys.str[querys.query_index] != ']' && querys.query_index < querys.str.length) {
        querys.query_index++;
    }
    param = querys.str.substr(start_index, querys.query_index - start_index);
    param = param.trim();
    var ret = {};
    if (param.length == 0) {
        ret.error_message = "You need to give a parameter.";
        ret.query_index = querys.query_index;
    }
    ret.param = param;
    return ret;
}

function query_open_bracket_look(querys) {
    if (querys.str[querys.query_index] == '[') {
        return 1;
    }
    return 0;
}


function query_bracket(querys) {
    var bracket = {};

    if (querys.str[querys.query_index] != '[') {
        return {
            'error_message': 'Expecting: [',
            'query_index': querys.query_index
        };
    }
    querys.query_index++;

    // get a full or partial bracket_type name
    var res;
    var bracket;
    var bracket_keyword;
    bracket.bracket_type = "";
    while ((res = query_matches_bracket_type(bracket.bracket_type)) == "PARTIAL") {
        query_whitespace(querys);
        bracket_keyword = query_identifier(querys, just_look = 1);
        if (bracket_keyword.error_message) break;

        bracket_keyword = query_identifier(querys, just_look = 0);
        if (bracket.bracket_type) bracket.bracket_type = bracket.bracket_type + " ";
        bracket.bracket_type = bracket.bracket_type + bracket_keyword.keyword.toLowerCase();
    }

    if (res == "ERROR") {
        bracket.error_message = "This is not a command: " + bracket_keyword.keyword;
        bracket.error_index = querys.query_index;
        bracket.error_type = "bad_command";
        return bracket;
    }

    // there are always potential autocompletes even on a full match.
    var matches = query_matches_bracket_type_search(bracket.bracket_type);
    bracket.autocomplete = matches;


    query_whitespace(querys);

    if (query_bracket_type_info[bracket.bracket_type]) {
        bracket.bracket_param = query_get_bracket_param(querys);

        bracket_params_autocomplete = query_bracket_params_autocomplete_lookup(bracket.bracket_type, bracket.bracket_param);

        if (bracket_params_autocomplete.length > 0) {
            bracket.autocomplete = bracket_params_autocomplete;
        }

        if (!bracket.bracket_param.param.length) {
            bracket.error_message = "Expecting: " + query_bracket_type_info[bracket.bracket_type];
            bracket.error_index = querys.query_index;

            return bracket;
        }
        query_whitespace(querys);
    }


    if (querys.str[querys.query_index] != ']') {
        bracket.error_message = 'Expecting: ]';
        bracket.error_index = querys.query_index;
        return bracket;
    }

    querys.query_index++;

    return bracket;
}

function query_matches_bracket_type(bracket_type) {
    // bracket_type = bracket_type.trim();
    if (!bracket_type.length) return "PARTIAL";
    var cap_bracket_type = bracket_type.toLowerCase();
    for (var i = 0; i < query_bracket_types.length; i++) {
        if (query_bracket_types[i] == cap_bracket_type) {
            return "FULL";
        }
        if (query_bracket_types[i].indexOf(cap_bracket_type) == 0) {
            return "PARTIAL";
        }
    }
    return "ERROR";
}

function query_matches_bracket_type_search(bracket_type) {
      bracket_type = bracket_type.trim();
  //if ( ! bracket_type.length ){} return query_bracket_types;
  var cap_bracket_type = bracket_type.toLowerCase();
  var matches = [];
  for( var i=0; i < query_bracket_types.length; i++ ){
    if ( query_bracket_types[ i ].indexOf(cap_bracket_type) == 0 ){
    	
     	if(query_bracket_types[ i ] == "my tags" || query_bracket_types[ i ] == "my vaults" ){
    		matches.push( "[" + query_bracket_types[ i ] + "]" );
    	}else{
    		matches.push( "[" + query_bracket_types[ i ] );
    	}
      
    }
  }
  return matches;
}