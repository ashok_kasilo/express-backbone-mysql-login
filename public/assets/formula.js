var fr_identifier_chars = {
    "_" : "1"
  , "1" : "1"
  , "2" : "1"
  , "3" : "1"
  , "4" : "1"
  , "5" : "1"
  , "6" : "1"
  , "7" : "1"
  , "8" : "1"
  , "9" : "1"
  , "0" : "1"
  , "a" : "1"
  , "b" : "1"
  , "c" : "1"
  , "d" : "1"
  , "e" : "1"
  , "f" : "1"
  , "g" : "1"
  , "h" : "1"
  , "i" : "1"
  , "j" : "1"
  , "k" : "1"
  , "l" : "1"
  , "m" : "1"
  , "n" : "1"
  , "o" : "1"
  , "p" : "1"
  , "q" : "1"
  , "r" : "1"
  , "s" : "1"
  , "t" : "1"
  , "u" : "1"
  , "v" : "1"
  , "w" : "1"
  , "x" : "1"
  , "y" : "1"
  , "z" : "1"
  , "A" : "1"
  , "B" : "1"
  , "C" : "1"
  , "D" : "1"
  , "E" : "1"
  , "F" : "1"
  , "G" : "1"
  , "H" : "1"
  , "I" : "1"
  , "J" : "1"
  , "K" : "1"
  , "L" : "1"
  , "M" : "1"
  , "N" : "1"
  , "O" : "1"
  , "P" : "1"
  , "Q" : "1"
  , "R" : "1"
  , "S" : "1"
  , "T" : "1"
  , "U" : "1"
  , "V" : "1"
  , "W" : "1"
  , "X" : "1"
  , "Y" : "1"
  , "Z" : "1"
};

var fr_token_table = {
    "1" : "1"
  , "2" : "1"
  , "3" : "1"
  , "4" : "1"
  , "5" : "1"
  , "6" : "1"
  , "7" : "1"
  , "8" : "1"
  , "9" : "1"
  , "0" : "1"
  , "=" : "="
  , ">" : ">"
  , "<" : "<"
  , "<=" : "L"
  , ">=" : "G"
  , "><" : "D" // different
  , "<>" : "D"
  , "AND" : "A"
  , "OR" : "O"
  , "XOR" : "X"
  , "NOT" : "N"
  , "P" : "P"
  , "M" : "M"
  , "+" : "+"
  , "-" : "-"
  , "*" : "*"
  , "/" : "/"
  , "(" : "("
  , ")" : ")"
  , "\"" : "\""
  , "'" : "'"
  , "," : ","
  , "Identifier" : "I"
  , "String" : "S"
  , "END OF PROGRAM" : "Z"
};

var fr_function_list = {
  'power' : {
	    'parameters' : [ {'param_name' : 'unit', 'param_type' : 'number' }, {'param_name' : 'multiplier', 'param_type' : 'number' } ], 
	    func : fr_calc_power 
  },
  'factorial' : {
	  'parameters' : [ {'param_name' : 'n', 'param_type' : 'number' }, ], 
	  'func' : fr_calc_easy_func ,
	  'func_settings' : { 'helper_func' : fr_calc_easy_factorial }
  },
  'repeat' : {
	  'parameters' : [ {'param_name' : 'repeat_string', 'param_type' : 'string' }, {'param_name' : 'repeat_num', 'param_type' : 'number' } ], 
	  'func' : fr_calc_easy_func ,
	  'func_settings' : { 'helper_func' : fr_calc_easy_repeat }
  },
  'sum' : {
	  'parameters' : [ {'param_name' : 'values', 'param_type' : 'number', 'take_additional_params_as_list' : 'Y'  } ], 
	  'func' : fr_calc_easy_func ,
	  'func_settings' : { 'helper_func' : fr_calc_easy_sum }
  } 
  
};


function fr_tok_get_next_token(the_code, state){
  if ( ! state ){
    state = {};
    state.next_char = 0;
    state.token_start_char = 0;
    state.token_end_char = 0;
  } else {
    // we clone the state passed in so we can get a new state that we can change.
    // in the next version the state and the stream will be separated and so we won't have to clone. TODO
    state = JSON.parse(JSON.stringify(state));
    state.error_message = '';
    state.error_code = '';
  }
  state.token_code = '';

  // look for the next non white space which is the beginning of the next token.
  while(
    state.next_char < the_code.length
    &&
    ( 
         the_code[ state.next_char ] == ' ' 
      || the_code[ state.next_char ] == '\t' 
      || the_code[ state.next_char ] == '\n' 
      || the_code[ state.next_char ] == '\r' 
    )
  ){
      state.next_char++ ;
  }

  if ( state.next_char == the_code.length ){
    // we reached the end of the program/string
    state.token_code = 'Z';
    state.token_string = '';
    return state;
  }

  state.token_start_char = state.next_char;
  
  if ( ! fr_token_table[ the_code[ state.token_start_char ] ] && ! fr_identifier_chars[ the_code[ state.token_start_char ] ] ){
    state.error_message = "There is no token that starts with a " + the_code[ state.token_start_char ]; 
    return state;
  }

  if ( 
    the_code[ state.token_start_char ] >= '0' 
    && 
    the_code[ state.token_start_char ] <= '9' 
  ){
   
    // parse the whole number
    used_the_dot = false;
    while(
      state.next_char < the_code.length
      &&
      (
      the_code[ state.next_char ] >= '0' 
      && 
      the_code[ state.next_char ] <= '9' 
      ||
      the_code[ state.next_char ] == '.' && ! used_the_dot
      )
    ){
      if ( the_code[ state.next_char ] == '.' ) used_the_dot = true;
      state.next_char++;
    }
    state.token_code = '1';
  } else if( 
    the_code[ state.token_start_char ] == '>' && the_code[ state.token_start_char + 1 ] == '<' 
    ||
    the_code[ state.token_start_char ] == '<' && the_code[ state.token_start_char + 1 ] == '>' 
  ){
    state.next_char += 2;
    state.token_code = 'D';
  } else if ( the_code[ state.token_start_char ] == '"' || the_code[ state.token_start_char ] == "'"  ) {
    // else it's a string. so let's get the string as a single token
    var string_char = the_code[ state.token_start_char ];
    state.token_start_char++;  // the " isn't part of the token.
    state.next_char++;
    state.token_code = 'S';
    while( the_code[ state.next_char ] != string_char && state.next_char < the_code.length ){
       state.next_char++;
    }
    if ( state.next_char == the_code.length || the_code[ state.next_char ] != string_char ){ 
       state.error_message = "You started a string of characters with a " + string_char + " and you did not end it with another " + string_char + ".";
       state.error_code = "BADSTR";
    }
  } else if ( 
    fr_token_table[ the_code[ state.token_start_char ] ]
  ){
    // else it is one of the single char tokens specified in the fr_token_table.
    state.next_char++;
    state.token_code = the_code[ state.token_start_char ];
  } else {
    // it is an identifier or a multi-char reserved word.
    var token_code = false;
    if       ( the_code[ state.token_start_char ] == '>' && the_code[ state.token_start_char + 1 ] == '=' ){
      state.token_code = 'G';
      state.next_char += 2;
    } else if( the_code[ state.token_start_char ] == '<' && the_code[ state.token_start_char + 1 ] == '=' ){
      token_code = 'L';
      state.next_char += 2;
    } else if( the_code[ state.token_start_char ] == '>' && the_code[ state.token_start_char + 1 ] == '<' ){
      token_code = 'D';
      state.next_char += 2;
    } else {
      /* it's a keyword or identifier */
      while(
        state.next_char < the_code.length
        &&
        fr_identifier_chars[ the_code[ state.next_char ] ]
      ){
        state.next_char++;
      }

      state.token_end_char = state.next_char - 1;
      state.token_string = the_code.substring(state.token_start_char, state.token_end_char + 1);

      if ( fr_token_table[ state.token_string ] ){
         // it's one of the reserved tokens
         state.token_code = fr_token_table[ state.token_string ];
      } else {
         // we have an identifier. 
         state.token_code = 'I';
      }
    }
  }

  state.token_end_char = state.next_char - 1;
  state.token_string = the_code.substring(state.token_start_char, state.token_end_char + 1);

  if ( state.token_code == "S" ){
    // strings don't show the string char as part of the token_string and so the next_char 
    // needs to be pushed one spot forward or we will continue parsing from the close "
    state.next_char++;
  }

  return state;
}





function fr_parse_program(char_stream){
   var program = {};
   program.node_code = 'C'; // The Code
   program.left = fr_parse_expression(char_stream); // where's the second parameter

   return program;
}

function fr_parse_expression( char_stream, cur ){
   var ret = fr_tok_get_next_token(char_stream, fr_parse_last(cur));
   if ( 
         '1' == ret.token_code
      || 'I' == ret.token_code
      || '(' == ret.token_code
      || 'N' == ret.token_code
      || '+' == ret.token_code
      || '-' == ret.token_code
      || 'S' == ret.token_code
   ){
      return fr_parse_or(char_stream, fr_parse_last(cur));
   }

   ret.error_message = "Expecting a number, parenthesis, or function.";
   ret.error_code = "ENPF";
   return ret;
}

function fr_parse_or(char_stream, cur){
  var left = fr_parse_and_xor(char_stream, cur);
  var operator = fr_tok_get_next_token(char_stream, fr_parse_last(left));
  if ( "O" != operator.token_code ){
    // this is not a fr_boolean expression.
    return left;
  }

  operator.right = fr_parse_and_xor(char_stream, fr_parse_last(operator));

  operator.left = left;

  return operator;
}



function fr_parse_and_xor(char_stream, cur){
  var left = fr_parse_not(char_stream, cur);

  var operator = fr_tok_get_next_token(char_stream, fr_parse_last(left));
  if (
    ! (
       "A" == operator.token_code 
    || "X" == operator.token_code
    )
  ){
    // this is not a fr_boolean expression.
    return left;
  }

  operator.right = fr_parse_not(char_stream, fr_parse_last(operator));

  operator.left = left;

  return operator;
}

function fr_parse_not(char_stream, cur){
  var operator = fr_tok_get_next_token(char_stream, cur);

  if (
       "N" != operator.token_code 
  ){
    // this is not a NOT expression.
    return fr_parse_comparison(char_stream, cur);
  }

  operator.left = fr_parse_not(char_stream, fr_parse_last(operator));

  return operator;
}


function fr_parse_comparison(char_stream, cur){
  var left = fr_parse_plus_minus(char_stream, cur);

  var operator = fr_tok_get_next_token(char_stream, fr_parse_last(left));
  if (
    ! (
       ">" == operator.token_code 
    || "<" == operator.token_code
    || "L" == operator.token_code
    || "G" == operator.token_code 
    || "D" == operator.token_code 
    || "=" == operator.token_code 
    )
  ){
    // it's just a number.
    return left;
  }

  operator.right = fr_parse_plus_minus(char_stream, fr_parse_last(operator));

  operator.left = left;

  return operator;

}


// left is the md right is the operator and the next md to its left.
//   1 + 2 + 3*3 - 2 - 1
function fr_parse_plus_minus(char_stream, cur){
  var md = fr_parse_multiply_divide(char_stream, cur);

  var operator = fr_tok_get_next_token(char_stream, fr_parse_last(md));
  if ( ! ( '+' == operator.token_code || '-' == operator.token_code ) ) return md; 
  
  var op_array = {'token_code' :  '+', 'children' : new Array() };

  op_array.children.push(md);

  while( '+' == operator.token_code || '-' == operator.token_code ){
    if ( operator.token_code == '+' ){
      operator.token_code = 'P'; // unary plus
    } else {
      operator.token_code = 'M'; // unary minus
    }

    operator.left = fr_parse_multiply_divide(char_stream, fr_parse_last(operator));

    op_array.children.push( operator );

    operator = fr_tok_get_next_token(char_stream, fr_parse_last(operator.left));
  }

  return op_array;
}


function fr_parse_multiply_divide(char_stream, cur){
  var operand = fr_parse_operand(char_stream, fr_parse_last(cur));

  var operator = fr_tok_get_next_token(char_stream, fr_parse_last(operand));
  if ( ! ( '*' == operator.token_code || '/' == operator.token_code ) ) return operand; 
  
  var op_array = {'token_code' :  '*', 'children' : new Array() };

  op_array.children.push(operand);

  while( '*' == operator.token_code || '/' == operator.token_code ){
    if ( operator.token_code == '*' ){
      operator.token_code = 'P'; // Positive 
    } else {
      operator.token_code = 'R'; // reciprocal
    }

    operator.left = fr_parse_operand(char_stream, fr_parse_last(operator));

    op_array.children.push( operator );

    operator = fr_tok_get_next_token(char_stream, fr_parse_last(operator.left));
  }

  return op_array;
}


 
function fr_parse_operand(char_stream, cur){
   var ret = fr_tok_get_next_token(char_stream, fr_parse_last(cur));

   if ( ret.token_code == '1' ){
     return ret;
   }

   if ( ret.token_code == '(' ){
      return fr_parse_parenthesis(char_stream, cur);
   }

   if ( ret.token_code == '+' || ret.token_code == '-' ){
      if ( ret.token_code == '+' ){
        ret.token_code = 'P'; // unary plus
      } else {
        ret.token_code = 'M'; // unary minus
      }
      ret.left = fr_parse_operand(char_stream, ret);
      return ret;
   }

   if ( ret.token_code == 'I' ){
      var parenthesis = fr_tok_get_next_token(char_stream, ret);
      if ( parenthesis.token_code == '(' ){
        ret.token_code = 'F'; // this identifier is a function being called.
        ret.left = fr_parse_list(char_stream, ret);
      } else {
        ret.token_code = 'V'; // this identifier is a variable being used.
      }
      return ret;
   }
   if ( ret.token_code == 'S' ){
      return ret;
   }

   ret.error_message = "I expected a number or a parenthesis.";
   return ret;
}

function fr_parse_list(char_stream, cur){
   var parenthesis = fr_tok_get_next_token(char_stream, fr_parse_last(cur));
   if ( parenthesis.token_code != '(' ){
     parenthesis.error_message = "I expected a list which starts with an open parenthesis (, but I don't see the open parenthesis.";
     return parenthesis;
   }

   parenthesis.children = [];
   var list_item = false;
   var last_node = parenthesis;
   while(true){
     list_item = last_node = fr_parse_expression(char_stream, fr_parse_last(last_node));

     if ( list_item.error_code == 'ENPF' ){
       if ( list_item.token_code == ')' ){
         list_item.error_code = false;
         list_item.error_message = false;
         parenthesis.left = list_item;
       } else {
         parenthesis.error_child = list_item;
         parenthesis.error_message = "I expected a close parenthesis to end this list.";
       }
       break;
     }


     parenthesis.children.push(list_item); // add a list element


     // look for a comma or an end of list.
     var comma = fr_tok_get_next_token(char_stream, fr_parse_last(last_node));
     if ( comma.token_code == ')' ){
       // do nothing. let the ) code at the top of the loop handle this.
     } else if ( comma.token_code != ','  ){
       // uh it is not a comma.
       parenthesis.error_node = comma;
       parenthesis.error_message = "You need to put a , between items in a list.";
       return parenthesis;
     } else { // this is a comma. good.
       last_node = comma;
     }

   }
 
   // print(JSON.stringify(parenthesis, null, "  "));

   return parenthesis;
}



function fr_parse_parenthesis(char_stream, cur){
   var parenthesis = fr_tok_get_next_token(char_stream, fr_parse_last(cur));
   if ( parenthesis.token_code != '(' ){
     parenthesis.error_message = "Expected a parenthesis";
     return parenthesis;
   }
   parenthesis.children = [];

   parenthesis.children.push({ 'left' : fr_parse_or(char_stream, fr_parse_last(parenthesis)) });

   parenthesis.children[0].right = fr_tok_get_next_token(char_stream, fr_parse_last(parenthesis.children[0].left));
   if ( ! parenthesis.children[0].right || parenthesis.children[0].right.token_code != ')' ){
     parenthesis.children[0].right.error_message = "Expected a close parenthesis.";
   }
   return parenthesis;
}

// TODO use a stream that is separate from the tree, and then this func is eliminated.
function fr_parse_last(node, depth){
  if ( typeof node === "undefined" ) return false;
  if ( ! node ) return node;
  if ( typeof depth == "undefined" ) depth = 0;
  depth++;
  var right_last = false;
  var left_last = false;
  var child_last = false;

  if ( node.children && node.children.length ){
    var last = fr_parse_last(node.children[0]);
    for(var i=1; i < node.children.length; i++){
       var child_last = fr_parse_last(node.children[i]);
       if ( last.next_char < child_last.next_char ) last = child_last;
    }
    child_last = last;
  }

  if ( node.right && ! node.right.error_message ){
    return fr_parse_last(node.right, depth);
  }
  if ( node.left && ! node.left.error_message ){
    left_last = fr_parse_last(node.left, depth);
  }
  if ( right_last && left_last ){
    if ( left_last.next_char > right_last.next_char ) return left_last;
    else return right_last;
  } else if ( left_last ) return left_last;
  else if ( right_last ) return left_last;
  else if ( child_last ) return child_last;
  else return node; 
}
 
function fr_run_code(node){
   if ( ! node || ! node.left ) return false;
   return fr_run_expression(node.left);
}

function fr_run_expression(node){
   switch(node.token_code){
      case 'P':
         return Number(fr_run_expression(node.left));
      case 'M':
         return - Number(fr_run_expression(node.left));
      case 'R':  // reciprocal.
         return 1 / Number(fr_run_expression(node.left));
      case '-': // there is no - node. it's just part of + and we just treat the - as M (unary minus)
      case '+': // plus minus are left associative and so it's easier to use a loop.
         var retval=0;
         for(var i=0; i < node.children.length; i++){
            retval += Number(fr_run_expression(node.children[i]));
         }
         return retval;

      case '*': // * and / are left associative and so it's easier to use a loop.
      case '/':
         var retval=1;
         for(var i=0; i < node.children.length; i++){
            retval *= Number(fr_run_expression(node.children[i]));
         }
         return retval;


      case '>':
         return fr_boolean(Number(fr_run_expression(node.left)) > Number(fr_run_expression(node.right)));
      case '<':
         return fr_boolean(Number(fr_run_expression(node.left)) < Number(fr_run_expression(node.right)));
      case 'G':
         return fr_boolean(Number(fr_run_expression(node.left)) >= Number(fr_run_expression(node.right)));
      case 'L':
         return fr_boolean(Number(fr_run_expression(node.left)) <= Number(fr_run_expression(node.right)));
      case '=':
         return fr_boolean(Number(fr_run_expression(node.left)) == Number(fr_run_expression(node.right)));
      case 'D':
         return fr_boolean(Number(fr_run_expression(node.left)) != Number(fr_run_expression(node.right)));

      case 'A':
         return fr_boolean(fr_run_expression(node.left) && fr_run_expression(node.right));
      case 'O':
         return fr_boolean(fr_run_expression(node.left) || fr_run_expression(node.right));
      case 'X':
         return fr_boolean(fr_run_expression(node.left) != fr_run_expression(node.right));

      case 'N': /* NOT */
         return fr_boolean(! fr_run_expression(node.left)) ;

      case '(':
         return fr_run_expression(node.children[0].left);
      case '1':
         return Number(node.token_string);
      case 'F':
	 if ( fr_function_list[ node.token_string ] ){
            return fr_function_list[ node.token_string ].func( node.token_string, node.left ); // calls simple_func
         } else {
            return "The function is not defined.";
         }
      case 'S':
         return node.token_string;
      case 'V':
         return "It looks like you tried using an identifier as a variable but I don't support that.";
      case 'I': /* Identifier */
         /* is this a function or a variable */
      default:
         return "Invalid expression: " + node.token_code;
   }
}

function fr_boolean(bval){
  return bval ? 1 : 0;
}

// to find an error you need to traverse the whole parse tree.
// because the error might be in node inside the tree.
function fr_error_check(node){
  if ( ! node ) return false;
  if ( node.error_message ){
    return node;
  }
  var error_node = false;
  if ( node.left ) error_node = fr_error_check(node.left);
  if ( error_node ) return error_node;
  if ( node.right ) error_node = fr_error_check(node.right);
  if ( error_node ) return error_node;
  if ( node.children && node.children.length )
  for(var i=1; i < node.children.length; i++){
    error_node = fr_error_check(node.children[i]);
    if ( error_node ) return error_node;
  }
  return false;
}

function fr_run_check_params(func_name, argument_list){
  if ( ! fr_function_list[func_name].parameters ) return false;

  var func_arguments = 0;
  if ( argument_list && argument_list.children && argument_list.children.length ) func_arguments = argument_list.children.length;

  if ( fr_function_list[func_name].parameters[fr_function_list[func_name].parameters.length - 1].take_additional_params_as_list != 'Y' ){
    var max_params = fr_function_list[func_name].parameters.length + fr_function_list[func_name].parameters[fr_function_list[func_name].parameters.length - 1].amount - 1;

    if ( func_arguments > max_params ) return "You have " + func_arguments + " arguments for function " + func_name + " but the function accepts at most " + max_params + " arguments.";
  }

  for(var param_i = 0; param_i < fr_function_list[func_name].parameters.length; param_i++){
    for ( p_num = 0; p_num < fr_function_list[    func_name].parameters[param_i].amount; p_num++){
      var argument_i = param_i + p_num;
      if ( argument_i >= func_arguments ) return "Missing argument " + fr_function_list[func_name].parameters[param_i].param_name + ". param number " + (argument_i + 1) + " in function " + func_name + ".";
    }
  }

  return false;
}

// the list is of expressions we need to run them to get the values.
function fr_run_get_arguments(func_name, argument_list){
   var arg_values = {};
   var last_param = -1;
   if ( ! argument_list || ! argument_list.children || ! argument_list.children.length ) return arg_values;
   for(var arg_i=0; arg_i < argument_list.children.length; arg_i++){
     var param_i = arg_i;
     if ( last_param >= 0 ) param_i = last_param;
     if( fr_function_list[func_name].parameters[param_i].take_additional_params_as_list == 'Y' ){
       last_param = param_i;
       if ( ! arg_values[ fr_function_list[func_name].parameters[param_i].param_name  ] ) arg_values[ fr_function_list[func_name].parameters[param_i].param_name  ] = [];
       arg_values[  fr_function_list[func_name].parameters[param_i].param_name  ].push( fr_run_expression(argument_list.children[arg_i]) );
     } else {
       arg_values[  fr_function_list[func_name].parameters[param_i].param_name ] = fr_run_expression(argument_list.children[arg_i]);
     }
   }
   return arg_values;
}

// a function definition that does everything explicitly. not recommended.
function fr_calc_power(func_name, argument_list){
  var ret = fr_run_check_params(func_name, argument_list);
  if ( ret ) return ret;
  arg_values = fr_run_get_arguments(func_name, argument_list);

  return Math.pow(arg_values.unit, arg_values.multiplier);
}

function fr_calc_easy_func(func_name, argument_list){
  var ret = fr_run_check_params(func_name, argument_list);
  if ( ret ) return ret;
  arg_values = fr_run_get_arguments(func_name, argument_list);
  
  return fr_function_list[func_name].func_settings.helper_func(arg_values);
}

// a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_factorial(args){
   var ret = 1;
   for(var n = args.n; n > 0; n--){
      ret = ret * n;
   }
   return ret;
}

// a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_repeat(args){
   ret_str = '';
   for(var n = args.repeat_num; n > 0; n--){
     ret_str = ret_str + args.repeat_string;
   }
   return ret_str;
}

// a function definition that uses fr_calc_easy_func to make it simpler.
function fr_calc_easy_sum(args){
//console.log(JSON.stringify(args, null, "  "));
   var ret = 0;
   for(var n = 0; n < args.values.length; n++){
      ret += args.values[n];
   }
   return ret;
}

