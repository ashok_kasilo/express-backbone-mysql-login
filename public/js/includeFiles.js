"use strict";
/**
 * @author Gidon Wise
 * @author Ashok k
 **/

var files = [
	"config/config-development.js",
	"Router.js",
	
	//helpers
	"helpers/MainHelper.js",
	"helpers/TextHelper.js",
	"helpers/DateHelper.js",
	"helpers/Utilities.js",
	"helpers/Messages.js",
	"helpers/queryconf.js",
	"helpers/ChartHelper.js",
	"helpers/HighlightHelper.js",
	
	//models
	"models/SignIn.js",
	"models/SignOut.js",
	"models/ForgotPassword.js",
	"models/PasswordReset.js",
	"models/Account.js",
	"models/User.js",
	"models/Vault.js",
	"models/Member.js",
	"models/Dataset.js",
	"models/Tag.js",
	"models/Labelset.js",
	"models/Label.js",
	"models/Datapoint.js",
	"models/Labelview.js",
	"models/Mashup.js",
	"models/Mashup_To_Item.js",
	"models/Highlights.js",
	"models/Highlight_To_User.js",
	"models/Highlight_To_Tag.js",
	"models/Tag_To_Dataset.js",
	"models/Tag_To_Mashup.js",
	//collections
	"collections/VaultsCollection.js", 
	"collections/MembersCollection.js", 
	"collections/TagsCollection.js", 
	"collections/DatasetsCollection.js",
	"collections/LabelsetsCollection.js",
	"collections/LabelsCollection.js",
	"collections/DatapointsCollection.js",
	"collections/MahsupItemsCollection.js",
	"collections/MashupsCollection.js",
	"collections/HighlightsCollection.js",
	"collections/UsersCollection.js",
	"collections/DP_VaulesCollection.js",
	//views
	"views/MenuView.js",
	"views/BreadcrumbsView.js",
	"views/PaginationView.js",
	"views/UserInfoView.js",
	"views/SignInView.js",
	"views/HomeView.js",
	"views/ForgotPasswordView.js",
	"views/ForgotPasswordStepsView.js",
	"views/ForgotPasswordResetView.js",
	"views/AccountView.js",
	"views/AccountEditView.js",
	"views/RegisterFormView.js",
	"views/QuickSearchView.js",
	"views/VaultsListView.js",
	"views/VaultView.js",
	"views/MembersListView.js",
	"views/DatasetView.js",
	"views/TagsListView.js",
	"views/SearchView.js",
	"views/LabelDataPointTemplateView.js",
	"views/CopyPasteLabels.js",
	"views/ViewDatasetView.js",
	"views/MashupEditView.js",
	"views/MashupView.js",
	"views/MashupItemsListView.js",
 	"views/LabelsetsListView.js",
	"views/LabelListView.js",
	"views/LabelsetModalView.js",
	"views/ViewLabelDataPointTemplateView.js",
	"views/MashupItemsListTableView.js",
	"views/ViewTagsTemplateView.js",
	"views/ViewUsersTemplateView.js",
	"views/ViewDatasetsInCalculationModalView.js",
	"views/MashupCalculationModalView.js",
 	"views/EmailSettingView.js",
	"views/UserProfileView.js",
	"views/SearchResultsView.js",
	"views/HighlightModalView.js",
	"views/AboutView.js",
	"views/ContactUsView.js",
	"views/TermsAndConditionsView.js",
	"views/HomeHeaderView.js" 
 ];

var templates = [
	"menu-view",
	"breadcrumbs-view",
	"user-info-signed",  
	"user-info-unsigned",  
	"sign-in-view",
	"home-view",
 	"forgot-password-view",
	"forgot-password-steps-view",
	"forgot-password-reset-view",
	"account-view",
	"account-edit-view",
	"register-form-view",
	"countries-view",
	"quick-search-view",
	"vault-view",
	"vaults-list-view",
	"members-list-view",
	"pagination-view",
	"tags-list-view",
	"search-view",
	"search-results-view",
	"label-datapoint-template-view",
	"copy-and-paste-labels-view",
	"dataset-view",
	"dataset-edit-view",
	"mashup-view",
	"mashup-edit-view",
	"mashup-itemslist-view",
 	"labelsets-list-view",
	"label-list-view",
	"labelset-modal-view",
	"view-label-datapoint-template-view",
	"mashup-itemslist-table-view",
	"view-tags-template-view",
	"view-users-template-view",
	"view-datasets-in-calculation-modal-view",
	"mashup-calculation-modal-view",
 	"email-setting-view",
	"user-profile-view",
	"highlight-modal-view",
	"about-view",
	"contact-us-view",
	"terms-and-conditions-view",
	"home-header-view" 
	 
];

/*
 * Include one JavaScript file to head
 * @param string filename 
 */
var allFiles = "";
function includeFile(filename) {
	$("head").append("<script type=\"text/javascript\" src=\"js/app/"+ filename +"\" />");
}

/*
 * Include all files in array
 * @param array Array with filenames
 */
function includeFiles(array) {
	$.each(array, function(index, value) {
		includeFile(value);
	});	
}

/*
 * Include template
 * @param string id 
 * @param string filename
 */
function includeTemplate(filename) {
	$.ajax({
		async: false,
		url: "js/app/templates/"+filename+".html", 
		dataType : "html",
		success: function(data) {
			$("head").append("<script type=\"text/template\" id=\""+ filename +"\">"+data+"</script>");
		}
	});
	
	
}

/*
 * Include all files in array
 * @param array array Array with filenames
 */
function includeTemplates(array) {
	$.each(array, function(index, value) {
		includeTemplate(value);
	});	
}