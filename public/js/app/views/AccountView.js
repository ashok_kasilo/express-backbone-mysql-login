"use strict";
/**
 * @author Gidon Wise
 * @author Ashok k
 **/

window.AccountView = Backbone.View.extend({ 
	template:  _.template($("#account-view").html()),

	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		return this;
	},
 
	events: {
		"click #edit": "showForm"
	},

	showForm: function() {
		app.accountEditView = new AccountEditView({ model : this.model });
		$('#main-area').empty().append('<div class="info-row" id="messages"  ></div>');
		$("#main-area").append(app.accountEditView.render().el);
		console.log(this.model.get("user_wants_highlight_emails"));
		if(window.sess.user_wants_highlight_emails == "N"){
			$("#stop_highlight_emails" ).prop( "checked", true );
		}else{
			$("#stop_highlight_emails" ).prop( "checked", false );
		}
	}
});