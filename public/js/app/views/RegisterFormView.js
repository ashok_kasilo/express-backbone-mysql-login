"use strict";
/**
 * @author Gidon Wise
 * @author Ashok k
 */

window.RegisterFormView = Backbone.View
		.extend({
			template : _.template($("#register-form-view").html()),

			render : function() {
				$(this.el).html(this.template(this.model.toJSON()));
				return this;
			},

			events : {
				"submit #register-form" : "register"
			},

			/**
			 * Event click #register-button // updates
			 */
			register : function() {

				TextHelper.hideErrors();
				console.log(" --- ::", $("#username").val());
				this.model.set({
					mobile_number : $("#mobile_number").val().trim(),
					user_email : $("#user_email").val().trim(),
					user_password : $("#user_password").val().trim(),
					confirm_password : $("#confirm_password").val().trim(),
					user_first_name : $("#user_first_name").val().trim(),
					user_last_name : $("#user_last_name").val().trim()
				});
				console.log(" --- ::", this.model);
				this.model.save(null, {
					async : false
				});

				var hasNoErrors = _.isUndefined(this.model.get("errors"));
				if (hasNoErrors && this.model.get("status") == "ok") {

					if (app.hash === "") {

						MainHelper.navigate("");
						TextHelper.addMessage("Thank you for registering. Please verify your email to activate your account.","ok");
						TextHelper.showMessages();

					} else {

						var signIn = new SignIn({
							username : this.model.get("username"),
							user_password : this.model.get("user_password")
						});
						signIn.fetch({
							async : false
						});
					}
					return;
				} else {
					TextHelper.showErrors(this.model.get("errors"));
					this.model.unset("errors");
				}

				if (this.model.get("status") != "ok") {
					TextHelper.showMessages();
				}

			}

		});