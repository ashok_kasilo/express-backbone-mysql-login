"use strict";
/**
 * @author Gidon Wise
 * @author Ashok k
 **/

window.ForgotPasswordStepsView = Backbone.View.extend({

	template: _.template($("#forgot-password-steps-view").html()),

	render: function(eventName) {
		$(this.el).html(this.template());
		return this;
	}
});