"use strict";
/**
 * @author Gidon Wise
 * @author Ashok k
 **/

window.AccountEditView = Backbone.View.extend({ 
	template:  _.template($("#account-edit-view").html()),

	render: function() {
		$(this.el).html(this.template(this.model.toJSON()));
		return this;
	},
 
	events: {
		"submit #account-form": "save"
	},

	/**
	 * Event click #save-button
	 **/
	save: function (){
		TextHelper.hideErrors();

		this.model.set({
			user_email: $("#user_email").val().trim(),
			user_old_password: $("#user_old_password").val().trim(),
			user_password: $("#user_password").val().trim(),
			confirm_password: $("#confirm_password").val().trim(),
			user_first_name: $("#user_first_name").val().trim(),
			user_last_name: $("#user_last_name").val().trim(),
			user_wants_highlight_emails : $("#stop_highlight_emails").attr("checked") ? "N" : "Y"
		});
		Backbone.emulateHTTP = true;
		/*Backbone.emulateJSON = true;*/
		this.model.save(null, { async : false });

		if (!_.isUndefined(this.model.get("errors"))) {
			TextHelper.showErrors(this.model.get("errors"));
			this.model.unset("errors");
		}

		if (this.model.get("status") != "ok") {
			TextHelper.showMessages();
		} else {
			
			if($("#stop_highlight_emails").attr("checked")){
				
				app.vaults_to_user = new VaultsCollection({v2u_user_accepted : 'Y', user_id : window.sess.user_id });
				app.vaults_to_user.fetch({ async : false });
				_.each(app.vaults_to_user.models, function(vault) {
					Backbone.ajax({
						type: "PUT",
						url : BASEURL_API + "vault_to_user/update",
						data : { vault_id : vault.get("vault_id"), user_id : window.sess.user_id, v2u_wants_highlight_emails : "N", stoken : window.sess.stoken },
						success : function(response) {
								  if(response.status === 'ok'){
									  TextHelper.hideResponse();
								  }else{
									  TextHelper.addMessage("Unable to update vault to user. <br /> Error : "+response.message);
									  TextHelper.showMessages();
								  }
							}
					});
				});
				
			}
			
			window.sess.user_first_name = $("#user_first_name").val();
			window.sess.user_last_name = $("#user_last_name").val();
			window.sess.user_email = $("#user_email").val();
		//	window.sess.user_wants_highlight_emails = $("#stop_highlight_emails").attr("checked") ? "N" : "Y";
			MainHelper.navigate("#account");
		}
	}

});