"use strict";
/**
 * @author Ashok Kasilo
 */
var Messages = {
		
		// when user try to delete label set that is being used by other resources
		deleteLabelsetError : "You cannot delete this label set as long as it is being used by data sets."
		
};