"use strict";
/**
 * @author Gidon Wise
 * @author Ashok k
 */

var MainHelper = {

	navigate : function(hash) {
		if (hash == window.location.hash) {
			app.navigate("#reload", false);
		}
		app.navigate(hash, true);
	},

	checkRegister : function() {
		if (!MainHelper.isSigned()) {
			app.hash = window.location.hash;
			app.navigate("sign-in", true);
			app.deleteMessages = false;

			TextHelper.findMessage({
				status : "error",
				message : "Please sign in."
			});

			return false;
		}
		return true;
	},

	isSigned : function() {
		if (!window.sess || _.isUndefined(window.sess)) {

			return false;
		}
		return true;
	},

	showMenus : function(params) {

		if (_.isUndefined(params)) {
			params = {};
		}

		if (_.isUndefined(params.active)) {
			params.active = "";
		}

		if (_.isUndefined(params.menus)) {
			params.menus = [ {
				name : "main",
				place : "navbar"

			} ];
		}
		_.each(params.menus, function(item) {
			var menu = new MenuView({
				"menu" : item.name,
				"active" : params.active
			});
			$("#" + item.place).html(menu.render().el);
		});
	},
	hideMenus : function() {
		$('#search_area').empty();
		$('#navbar').empty();
	},
	showNavs : function(navItems) {
		var nav = new BreadcrumbsView({
			"items" : navItems.items,
			"active" : navItems.active
		});
		$("#userNavBar").html(nav.render().el);
		$("#search_area").html("");
	},
	hideNavs : function() {
		$('#userNavBar').empty();
	},
	redirectSignIn : function(signin) {

		MainHelper.hideNavs();

		app.hash = window.location.hash;
		document.title = "Sign In";
		app.signInView = new SignInView({
			model : signin
		});
		$('#main-area').empty().append('<div class="info-row" id="messages"  ></div>');
		$("#main-area").append(app.signInView.render().el);
	}
	
	 
};
