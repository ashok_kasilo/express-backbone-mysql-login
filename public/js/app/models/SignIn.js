"use strict";
/**
 * @author Gidon Wise
 * @author Ashok k
 **/
window.SignIn = Backbone.Model.extend({

	defaults : {
		username : "",
		user_password : ""
	},

	url : function() {
		return BASEURL_API + "account/sign_in";
	},
 
	/**
	 * Send post data to server for authorization. Set to cookie auth information.
	 **/
	sync : function(method, model, options) {
		options.type = "POST";
		options.data = {
			username : this.get("username"),
			user_password : this.get("user_password")
		};
		options.async = false;
		return Backbone.sync(method, model, options);
	},

	/**
	 * Get information about auth
	 **/
	parse : function(response) {
		$(".dropdown-menu").parent().removeClass('open');
		TextHelper.findMessage(response);
		
		if (response.status != "ok") {
			
			if (app.hash === "") {
				
				MainHelper.navigate("sign-in");
				TextHelper.addMessage(response.message,"error");
				TextHelper.showMessages();
				
			} else {
				MainHelper.redirectSignIn(new SignIn({
					username : this.get("username"),
					user_password : this.get("user_password")
				}));
				TextHelper.showMessages();
			}
			if(!_.isUndefined(response.errors)){
				TextHelper.showErrors(response.errors);
			}
			return;
			
		}
		
		window.sess = response.user;
		window.sess.stoken = response.stoken;
		$.cookie("sess", JSON.stringify(window.sess));
		MainHelper.navigate(app.hash);
	}

});