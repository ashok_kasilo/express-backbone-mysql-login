"use strict";
/**
 * DataNile Users Collection
 */

window.UsersCollection = Backbone.Collection.extend({
	
	model: window.User,
	url: function() {
		return BASEURL_API + "users"; 
	},
	sync: function(method, model, options){  
		 
		if(MainHelper.isSigned()){
			options.data = {
				stoken : window.sess.stoken
			};
		}
		return Backbone.sync(method, model, options);  
	},
	parse: function(response) {
		
		if (response.status == 'ok') {
			return response.recs;
		}
		return response;
		 
	} 

});