"use strict";
/**
 * @author Gidon Wise
 * @author Ashok k
 **/

var app;
var sess = false;
if ( $.cookie("sess") ){
  sess = JSON.parse($.cookie("sess"));
}

var dispatcher = _.clone(Backbone.Events);

// find browser 

//Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+

var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
// At least Safari 3+: "[object HTMLElementConstructor]"

var isChrome = !!window.chrome && !isOpera;              // Chrome 1+

var isIE = /*@cc_on!@*/false || !!document.documentMode;   // At least IE6

var fixHelper;
$(document).ready(function() {
	fixHelper = function(e, ui) {
	    ui.children().each(function() {
	        $(this).width($(this).width());
	    });
	    return ui;
	};
	app = new AppRouter();
	Backbone.history.on("route", function(params, name, callback) {
		app.go();
	});
	Backbone.history.start();
});
