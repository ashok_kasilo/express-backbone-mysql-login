"use strict";

/**
 * @author Gidon Wise
 * @author Ashok k
 */
var AppRouter = Backbone.Router.extend({
			statusMessage : "",
			relocationHash : "",
			messages : [],
			deleteMessages : true,
			breadcrumbStock : [],
			hash : "",
			vaults_to_user : [],
			vaults : [],
			data_sets : [],
			mashups : [],
			members : [],
			labelsets : [],
			label_view : null,
			tags : [],
			label_collection : [],
			chart_label_collection : [],
			datapoints_collection : [],
			vault_labelsets : [],
			mashupItems : [],
			mashupview : null,
			_user_vaults_forlabelsets : [] ,
			mashup : "",
			users : [],
			chart_data : {},
			dp_value_collection: [],
			results_view : null,
			search_results_collection : [],
			mashup_dps_collection : [],
			routes : {

				"" : "home",

				"account" : "account",
				"forgot" : "forgotPassword",
				"pw-reset/:user_id/:confirmation_code" : "forgotPasswordReset",
				"register" : "signUp",
				"confirm/:user_id/:email/:confirmation_code" : "confirm",
				"sign-out" : "signOut",
				"login" : "signIn",
 				"user-profile/:user_id" : "userProfile",

				// adding a default route - will match unknown routes to home route
				'*path':  'home'

			},

			/**
			 * call for every Route
			 */
			go : function() {

				
				TextHelper.showMessages();
				$("#home_page_search").val("");

				
			},

			/**
			 * Route: ""
			 */
			home : function() {

				if (MainHelper.isSigned()) {

					 MainHelper.navigate("search");

				}else{

					$("body").removeAttr('class');
					$("body").attr('class', 'whtbg');

					app.hash = "";
					document.title = "Home";

					MainHelper.hideNavs();

					app.view = new HomeView();
					$('#main-area').empty().append('<div class="" id="messages"  </div>');
					$("#main-area").append(app.view.render().el);

				}

			},

		 
			/**
			 * Route: account
			 */
			account : function() {

				$("body").removeClass("whtbg");

				app.hash = "";
				if (!MainHelper.checkRegister())
					return false;
				MainHelper.showMenus();
				document.title = "Account settings";

				$("#top_header_section").html("<div class='container'><h3>Account settings</h3></div>");
				$("#top_header_section").removeClass();
				$("#top_header_section").addClass("site-sectioninner");

				this.breadcrumbStock = [];
				this.breadcrumbStock.push("home");
				MainHelper.showNavs({
					items : this.breadcrumbStock,
					active : "Account settings"
				});

				app.account = new Account();
				app.account.set({
					user_id : window.sess.user_id,
					user_email : window.sess.user_email,
					user_first_name : window.sess.user_first_name,
					user_last_name : window.sess.user_last_name,
					username : window.sess.username
				});

				app.accountView = new AccountView({
					model : app.account
				});
				$('#main-area').empty().append('<div class="info-row" id="messages"  ></div>');
				$("#main-area").append(app.accountView.render().el);
			},

			/**
			 * Route: forgot
			 */
			forgotPassword : function() {

				$("body").removeClass("whtbg");

				app.hash = "";
				document.title = "Forgot Password";

				MainHelper.showMenus({
					active : "home"
				});

				$("#top_header_section").html("<div class='container'><h3>Forgot password</h3></div>");
				$("#top_header_section").removeClass();
				$("#top_header_section").addClass("site-sectioninner");

				this.breadcrumbStock = [];
				this.breadcrumbStock.push("home");
				MainHelper.showNavs({
					items : this.breadcrumbStock,
					active : "Forgot password"
				});

				var fPassword = new ForgotPassword({
					username : $("#username").val(),
					user_password : $("#user_email").val()
				});

				app.forgotPasswordView = new ForgotPasswordView({
					model : fPassword
				});
				$('#main-area').empty().append('<div class="info-row" id="messages"  ></div>');
				$("#main-area").append(app.forgotPasswordView.render().el);
			},

			/**
			 * Route: forgot-reset
			 */
			forgotPasswordReset : function(user_id, confirmation_code) {

				$("body").removeClass("whtbg");

				app.hash = "";
				MainHelper.showMenus();
				// TextHelper.setTitle("New Password Reset");
				document.title = "New password reset";

				$("#top_header_section").html("<div class='container'><h3>Password reset</h3></div>");
				$("#top_header_section").removeClass();
				$("#top_header_section").addClass("site-sectioninner");

				var passwordReset = new PasswordReset({
					user_id : user_id,
					confirmation_code : confirmation_code
				});
				passwordReset.fetch({
					async : false
				});
 				app.forgotPasswordResetView = new ForgotPasswordResetView({
					model : passwordReset
				});
 				$('#main-area').empty().append('<div class="info-row" id="messages"  ></div>');
				$("#main-area").append(app.forgotPasswordResetView.render().el);
			},

			forgotPasswordSteps : function() {

				$("body").removeClass("whtbg");

				$("#top_header_section").html("<div class='container'><h3>Forgot password steps</h3></div>");
				$("#top_header_section").removeClass();
				$("#top_header_section").addClass("site-sectioninner");

				app.forgotPasswordStepsView = new ForgotPasswordStepsView();
				$('#main-area').empty().append('<div class="info-row" id="messages"  ></div>');
				$("#main-area").append(app.forgotPasswordStepsView.render().el);
			},
			/**
			 * Route: sign-up
			 */
			signUp : function() {

				$("body").removeClass("whtbg");

				$("#top_header_section").html("<div class='container'><h3>Sign up</h3></div>");
				$("#top_header_section").removeClass();
				$("#top_header_section").addClass("site-sectioninner");

				app.hash = "";
				// TextHelper.setTitle("Sign Up");
				document.title = "Sign up";

				app.registerFormView = new RegisterFormView({
					model : new User()
				});
				$('#main-area').empty().append('<div class="panel panel-default col-sm-6 col-sm-offset-3" id="messages"  ></div>');
				$("#main-area").append(app.registerFormView.render().el);
			},

			/**
			 * Route : confirm
			 */
			confirm : function(uid, email, ccode) {

				app.hash = "";
				Backbone.ajax({
					type : "POST",
					url : BASEURL_API + "account/confirm/" + uid + "/" + email + "/" + ccode,
					success : function(response) {

						if (MainHelper.isSigned()) {
							var signOut = new SignOut();
							signOut.fetch({ async : false });

							app.deleteMessages = false;
							app.vaults = [];
						}

						app.hash = window.location.hash;

						if(response.status === 'ok'){
						  TextHelper.addMessage("Your account has been activated. Please login to continue.");
						  MainHelper.navigate("sign-in");
						}else{
						  TextHelper.addMessage("Unable to complete your registration process. <br /> Error : "+response.message);
						}

					}
				});
			},

 
			/**
			 * Route: sign-in
			 */
			signIn : function() {

				$("body").removeClass("whtbg");

				app.hash = "";
				// TextHelper.setTitle("Sign In");
				document.title = "Sign in";

				 
				var signin = new SignIn({
					username : $("#username_head").val(),
					user_password : $("#user_password_head").val()
				});

				app.signInView = new SignInView({
					model : signin
				});
				$('#main-area').empty().append('<div class="panel panel-default col-sm-6 col-sm-offset-3" id="messages"  ></div>');
				$("#main-area").append(app.signInView.render().el);
			},

			/**
			 * Route: sign-out
			 */
			signOut : function() {

				var signOut = new SignOut();
				signOut.fetch();
				app.deleteMessages = false;
				app.hash = "";
				MainHelper.navigate("");
			}
		 
		 

		});
