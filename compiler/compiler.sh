#!/bin/sh

s="--compilation_level SIMPLE_OPTIMIZATIONS"
s=$s" --js ../public/js/app/config/config-production.js"
for P in "*.js"; do
  
  for D in "../public/js/app/helpers/"; do
    for F in $D$P; do
      [ -r "$F" ] && s=$s" --js "$F
    done
  done
  
  for D in "../public/js/app/models/"; do
    for F in $D$P; do
      [ -r "$F" ] && s=$s" --js "$F
    done
  done
  
  for D in "../public/js/app/collections/"; do
    for F in $D$P; do
      [ -r "$F" ] && s=$s" --js "$F
    done
  done
  
  for D in "../public/js/app/views/"; do
    for F in $D$P; do
      [ -r "$F" ] && s=$s" --js "$F
    done
  done
  
  
done

s=$s" --js ../public/js/app/Router.js"
s=$s" --js ../public/js/app/application.js"
s=$s" --js_output_file ../public/js/datanile-min.js"
java -jar compiler.jar $s

