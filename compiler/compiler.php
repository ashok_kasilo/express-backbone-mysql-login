<?php

    fwrite(STDOUT, "development(d) or production(p): ");
    fscanf(STDIN, "%s\n", $param);

    if ($param == "d") {
        $param = "development";
    } elseif ($param == "p") {
        $param = "production";
    }
    $str = "";
    if ($param == "production") {
        shell_exec("sh compiler.sh");
       

        $path = "../public/js/app/templates/";
        $dir = opendir ($path);
        
        $str = "";
        while ($file = readdir ($dir)) 
        {
            $file = explode(".", $file);
            if ($file[1] == "html") {
                $str .= "<script id=\"{$file[0]}\" type=\"text/template\">\n";
                $str .= file_get_contents("{$path}{$file[0]}.{$file[1]}");
                $str .= "</script>\n";
            }
        }
        closedir ($dir);
    }

    if($param == "production"){
		$template = file_get_contents("../public/index-production.html"); 
    }elseif($param == "development"){
		$template = file_get_contents("../public/index-development.html"); 
    }
    

    $template = preg_replace("/<!--{$param}-->(.*)<!--\/{$param}-->/sU",'\\1', $template);
    $template = preg_replace("/<!--[a-z]+-->.*<!--\/[a-z]+-->/sU",'', $template);

    $template = str_replace("<!-- template -->", $str, $template);

    $template = "<!--THIS FILE WAS GENERATED. FOR CHANGES EDIT index.template.html-->\n" . $template;

    file_put_contents("../public/index.html", $template);
    fwrite(STDOUT, "index.html created\n");
