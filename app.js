/*
	Project start point.
	@author Ashok kasilo <ashok.k@logictreeit.com>
 */
var express = require('express'), cors = require('cors'), app = express(), config = require('./config.js'), expressValidator = require('express-validator');
 
app.use(express.bodyParser());
app.use(expressValidator);
app.use(express.static(__dirname + '/public'));
app.use(cors()); // automatically supports pre-flighting

require("./routes")(app);

// Adding cors 
app.use(function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	  next();
	});

app.disable('x-powered-by');

// Invalid request URI or Invalid Method
app.get('/*', function(req, res, next) {
	res.json([ {
		status : "error",
		message : 'Invalid API request!'
	} ]);
});
 
app.use(function(err, req, res, next) {
	
	if (err instanceof Error) {
		res.send({
			status : "error",
			message : err.message
		});
	} else {
		res.send({
			status : err.status,
			message : err.message,
			errors : err.errors
		});
	}

});
app.listen(config.port);

console.log("Application is running: " + config.url);
