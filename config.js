this.host = "localhost";
this.port = 8066;
this.url = "http://" + this.host + ":" + this.port;

// Database config
this.database = {
    host: "localhost",
    user: "root", // your username
    password: "root", // your password
    databaseName: "blueline" // database name
};

// Memcache config
this.memcache = {
    host: "localhost",
    port: "11211"
};

//SMTP config
this.smtp = {
    host: "smtp.sendgrid.net",
    user: "sudhakar_royal",
    password: "logictree"
};

//DataNile Links
this.links = {
		support : "Web Developer <ashok.inspires@gmail.com>",
		registrationConfirm : this.url + "/#confirm",
		invitation : this.url + "/#invite",
		PasswordReset : this.url + "/#pw-reset"
};

//DataNile Messages
this.messages = {
		registrationMailSubject : "Your New Blueline Account",
		invitationMailSubject : "Invitation to "
};

